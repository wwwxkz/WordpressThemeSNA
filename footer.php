<!-- Close container opened in header -->
</div>
<!-- Whatsapp floating icon -->
<a href="" class="whatsapp">
    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/whatsapp.png'; ?>"></img>
</a>
<!-- Footer -->
<div class="footer-container">
    <div class="footer">
        <div class="links">
            <div>
                <!-- Footer menu (links to pages) -->
                <ul class="links-lista">
                    <li><h5>Institucional</h5></li>
                    <li>Quem somos</li>
                    <li>Diretoria</li>
                    <li>Assembleias</li>
                    <li>Escritórios regionais</li>
                    <li>Links úteis</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Internacional</h5></li>
                    <li>ICAO</li>
                    <li>IFALPA</li>
                    <li>ITF</li>
                    <li>FLESPLA</li>
                    <li>CLAC</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Jurídico</h5></li>
                    <li>AJI - Assesoria Jurídica Individual</li>
                    <li>AJE - Assesoria Jurídica Emergencial</li>
                    <li>Artigos Jurídicos</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Notícias</h5></li>
                    <li>Latam</li>
                    <li>Avianca</li>
                    <li>Azul</li>
                    <li>Gol</li>
                    <li>Agrícola</li>
                    <li>Táxi aéreo</li>
                    <li>Aviação geral</li>
                    <li>Absa</li>
                    <li>Rio linhas aéreas</li>
                    <li>Passaredo</li>
                    <li>Brasília</li>
                    <li>Carreiras e oportunidades</li>
                </ul>
            </div>
            <div class="separator"></div>
            <div>
                <ul class="links-lista">
                    <li><h5>Leis, Convenções e Acordos</h5></li>
                    <li>Estatuto SNA</li>
                    <li>Nova Lei do aeronauta</li>
                    <li>CCT Aviação regular</li>
                    <li>CCT Táxi aéreo</li>
                    <li>CCT Agrícola</li>
                    <li>Portaria 3016/88</li>
                    <li>Lei 7183/84</li>
                    <li>CBA - Código Brasileiro de Aeronáutica</li>
                    <li>RBHA/RBAC</li>
                    <li>Portaria 6 do MTPS</li>
                    <li>Tabela de limite de jornada</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Saúde</h5></li>
                    <li>CAT Online</li>
                    <li>Apoio pscológico</li>
                    <li>Artigos de saúde</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Publicações</h5></li>
                    <li>Diário de Bordo</li>
                    <li>Newsletter</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Safety</h5></li>
                    <li>RELPREV/ASR</li>
                    <li>Artigos do safety</li>
                    <li>Emergência 24h</li>
                </ul>
                <ul class="links-lista">
                    <li><h5>Contato</h5></li>
                </ul>
            </div>
            <!-- Separator (whine line between divs) -->
            <div class="separator"></div>
            <!-- SNA and IFALPA logo -->
            <div class="logos">
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/logo.png'; ?>">
                </div>
                <br>
                <!-- Social icons (same as header) -->
                <div class="social">
					<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/instagram.svg'; ?>"></img> &nbsp &nbsp &nbsp
					<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/play.svg'; ?>"></img> &nbsp &nbsp &nbsp
					<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/facebook.svg'; ?>"></img> &nbsp &nbsp &nbsp
					<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/twitter.svg'; ?>"></img> &nbsp &nbsp &nbsp
					<img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/flickr.svg'; ?>"></img> &nbsp &nbsp &nbsp
				</div>
                <br><br>
                <h6>Membro</h6>
                <div>
                    <img src="<?php echo get_stylesheet_directory_uri() . '/assets/imgs/ifalpa.png'; ?>">
                </div>
            </div>
        </div>
    </div>
    <br>
    <!-- COPYRIGHT at the end of every page, as header and footer -->
    <h8>© COPYRIGHT 2022 - ALL RIGHTS RESERVED</h8>
</div>

</body>

</html>

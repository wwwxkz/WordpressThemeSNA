<?php get_header(); ?>

<?php the_title('<h2>','</h2>'); ?>

<?php the_content(); ?>

<?php get_footer(); ?>
